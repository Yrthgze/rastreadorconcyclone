import json
import gdata
import gdata.youtube
import gdata.youtube.service
import re
import time
import uuid
from twisted.python import log
html_script = "<script src=\"//cdn.embedly.com/widgets/platform.js\"></script>"

class youtubeListener():

    def __init__(self,track,cache,orderby='published',max_results='10',sleepTime=18):
        self.track=track[:]
        self.cache=cache
        self.client = gdata.youtube.service.YouTubeService()
        self.query = gdata.youtube.service.YouTubeVideoQuery()
        self.query.vq = str(track[0]) # the term(s) that you are searching for
        self.query.orderby = orderby  # how to display the results
        self.query.max_results = max_results
        self.last_id=0
        self.sleepTime=sleepTime
        self.running=0
        self.vToShowPerIter=int(max_results)/4
        self.videos=[]

    def start(self):
        self.running=1
        while self.running==1:
            self.getVideos()
            self.updateVideos(self.videos)
 
    def stop(self):
        self.running=0
        log.msg("Finishing youtube")
        return False

    def getVideos(self):
        time.sleep(2)
        videos=[]
        feed = self.client.YouTubeQuery(self.query)
        itera=0
        for entry in feed.entry:
            idStr=entry.id.text
            #Expresion regular para conseguir el id del video que es lo que interesa
            r = re.split('http://gdata.youtube.com/feeds/videos/',idStr)
            videoId = r[1]
            if self.last_id==videoId:
                videos=self.videos
                break
            if itera==0:
                self.last_id=videoId
                itera=1
            videos.append(videoId)
        self.videos=videos+self.videos

    def updateVideos(self,videos):
        for video in videos:
            log.msg("============================||")
            log.msg("got message from youtube")
            if self.running==1:
                yturl="http://www.youtube.com/v/"+video
                htmlcard="<a class=\"embedly-card\" href=\""+yturl+"\">Prueba</a>\n"
                nodeId=str(uuid.uuid4())
                htmlcard="<div class=\"node\" id=\"m"+nodeId+"\">"+htmlcard+"</div>"
                #twitter sample url
                htmlcard=htmlcard+html_script
                #htmlcard=TWITTER_SCRIPT_TAG % tweet_url
                node = {
                    "id": nodeId,
                    "html":htmlcard
                    }
                log.msg("Attemptim to update cache youtube")
                self.cache.update_cache(node)
                self.cache.send_updates(node)
                time.sleep(self.sleepTime)