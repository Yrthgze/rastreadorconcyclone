Feature: Compute factorial
  In order to see if cache is well

  Scenario: Update cache
    I have the html "<p>Hi!</p>"
    I update the cache
    I can see somewhere the html "<p>Hi!</p>"

  Scenario: Run listeners
    I have the listeners:
      | name      |
      | Youtube   |
      | Facebook  |
    I start the listeners
    I can see the listeners:
      | name      |
      | Youtube   |
      | Facebook  |

  Scenario: Get things
    I have the listeners2:
      | name      |
      | Youtube   |
      | Facebook  |
    I search for the keyword "Linkin Park"
    I get at least num 1
  Scenario: Everyone sees the changes
    I have the listeners2:
      | name      |
      | Youtube   |
      | Facebook  |
    I change cache with "Data changed"
    And everyones see the change
