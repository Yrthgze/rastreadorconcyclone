from flipy import Flipy
import datetime
import time
import uuid
from twisted.python import log
html_script = "<script src=\"//cdn.embedly.com/widgets/platform.js\"></script>"

class flickrListener():
    
    def __init__(self,track,cache,fmin,fmax):
        self.last_id=0
        self.last_owner=0
        dt = datetime.datetime.strptime(fmin, '%Y-%m-%d')
        t=time.mktime(dt.timetuple())
        self.min_taken_date = int(t)
        dt = datetime.datetime.strptime(fmax, '%Y-%m-%d')
        t=time.mktime(dt.timetuple())
        self.max_taken_date = int(t)
        self.api_key = '17c5788c2edf52d77e4593525ab17979'
        self.api_secret = '0f33b50af17d39e7'
        self.token = '72157629732416737-1a339cea75a5b04f'
        self.flickr = Flipy(self.api_key, self.api_secret)
        self.flickr.token = self.token
        self.cache=cache
        self.running=0
        self.track=track[:]
        self.limit=10

    def start(self):
        self.running=1
        while self.running==1:
            p=self.getAllPhotos()
            self.printPhotos(p)
            
    def stop(self):
        self.running=0
        return False

    def getAllPhotos(self):
        try:
            self.track[0]=str(self.track[0])
        except:
            pass
        photos=[]
        changed=0
        #log.msg("Today: "+str(self.min_taken_date))
        #log.msg("Tomorrow: "+str(self.max_taken_date))
        i=0
        for photo in self.flickr.photos.search.paginate(tags=self.track,min_upload_date=self.min_taken_date,max_taken_date=self.max_taken_date,sort='date-taken-desc'):
            if i>=self.limit:
                break
            if self.last_id==0:
                self.last_id=photo.id
                self.last_owner=photo.owner
            log.msg("Foto: "+str(photo.id))
            photos.append(photo)
            i=i+1
        return photos

    def printPhotos(self,photos):
        count=0
        for photo in photos:
            log.msg("============================<")
            log.msg("got message from flickr")
            if self.running==1:
                count+=1
                flickrurl="http://www.flickr.com/photos/"+str(photo.owner)+"/"+str(photo.id)
                htmlcard="<a class=\"embedly-card\" href=\""+flickrurl+"\">Prueba</a>\n"
                nodeId=str(uuid.uuid4())
                htmlcard="<div class=\"node\" id=\"m"+nodeId+"\">"+htmlcard+"</div>"
                #twitter sample url
                htmlcard=htmlcard+html_script
                node = {
                    "id": nodeId,
                    "html":htmlcard
                    }
                log.msg("Attemptim to update cache flickr")
                self.cache.update_cache(node)
                self.cache.send_updates(node)
                time.sleep(15)

    def checkNewPhotos(self):
        photos=[]
        for photo in self.flickr.photos.search.paginate(tags=self.track,min_upload_date=self.min_taken_date,max_taken_date=self.max_taken_date,sort='date-posted-desc'):
            if photo.id==self.last_id | photo.owner==self.last_owner:
                #log.msg("El primer id del checkeo es: "+str(photo.id))
                return False
            else:
                log.msg(self.last_id)
                log.msg(photo.id)
                photo.id=self.last_id
                photo.owner=self.last_owner
                photos.append(photo)
                #self.last_id=photo.id
                return photos