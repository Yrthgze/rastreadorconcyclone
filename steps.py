# -*- coding: utf-8 -*-
from lettuce import step
import datetime
import time
import os
import signal
import sys    
from lettuce import *
from dnowglobals import *
from nose.tools import assert_equals

world.cache=None
world.number=""
world.ne=None
world.new=[]


def kill():
    print('You pressed Ctrl+C!')
    pid=os.getpid()
    os.kill(pid,signal.SIGKILL)
def signal_handler(self,signal, frame):
        print('You pressed Ctrl+C!')
        kill()
signal.signal(signal.SIGINT, signal_handler)
@step('I have the html "(.*)"')
def have_the_html(step, html):
    world.html = str(html)
    world.cache = embedCache()
@step('I update the cache')
def update_the_cache(step):
    html={}
    html['html']=world.html
    world.cache.update_cache(html)

@step('I can see somewhere the html "(.*)"')
def see_somewher_html(step, html):
    assert_equals(str(html),world.cache.cache[0])


@step('I have the listeners:')
def have_the_html(step):
    ne={}
    i=0
    for dicti in step.hashes:
        netInner={}
        netInner['name']=step.hashes[i]['name']
        netInner['kw']="Test"
        ne[step.hashes[i]['name']]=netInner
        i=i+1
    world.cache = embedCache()    
    world.cache.runListeners(ne)
@step('I have the listeners2:')
def have_the_html(step):
    ne={}
    i=0
    for dicti in step.hashes:
        netInner={}
        netInner['name']=step.hashes[i]['name']
        netInner['kw']="Test"
        ne[step.hashes[i]['name']]=netInner
        i=i+1
    world.cache = embedCache()    
    world.ne=ne
@step('I start the listeners')
def star_the_cache(step):
    pass

@step('I can see the listeners:')
def see_listener_html(step):
    assert_equals(len(world.cache.listeners),2)
    world.cache.stopListeners()

@step('I search for the keyword "(.*)"')
def search_keyword(step,kw):
    print "hey yo: "+str(kw)
    for i in world.ne:
        world.ne[i]['kw']=str(kw)
    world.cache.runListeners(world.ne)

@step('I get at least num (\d+)')
def get_docs(step,mini):
    time.sleep(2)
    assert len(world.cache.cache) >= int(0)
    world.cache.stopListeners()

@step('I change cache with "(.*)"')
def change_cache(step,new):
    for i in world.ne:
        world.ne[i]['kw']="kw"
    world.cache.runListeners(world.ne)
    world.cache.stopListeners()
    world.new.append(new)
    world.cache.cache=world.new
@step('And everyones see the change')
def change_cache(step):
    for lis in world.cache.listenersInstances:
        assert_equals(world.cache.listenersInstances[lis].cache.cache[0],world.new[0])