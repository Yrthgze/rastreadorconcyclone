import uuid
import cyclone.escape
import os.path
from twisted.python import log
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import json

#####TWITTER KEYS#####
CONSUMER_KEY = 'ieZUZgZrSJJE0QLBBOsgXg'
CONSUMER_SECRET = 'PlIpSrh6unKYZISSDieBIFAB3D9f6aSh4p4Dmcn8Q'
OAUTH_TOKEN = '1015949947-0Akq5OBnEzTp7OwaIuvLNiKN6L52FNLVOW9yIyf'
OAUTH_TOKEN_SECRET = 'SJz3nXcyGt2lIKhmPiFg5VlTdHLbrRSPRRgUZ552xfe1e'

####Twitter auth handler####
auth = OAuthHandler(CONSUMER_KEY,CONSUMER_SECRET)
auth.set_access_token(OAUTH_TOKEN,OAUTH_TOKEN_SECRET)

###########Initialize the html file#################
html_script = "<script src=\"//cdn.embedly.com/widgets/platform.js\"></script>"
##Returns JSON in more legible human format
def pp(o):
   return json.dumps(o, indent=4)

class twitterListener(StreamListener):

    def __init__(self,track,async,cache):
        self.track = track[:]
        self.async = async
        self.cache = cache

    def start(self):
        log.msg("Tracking tweets...")
        twitterStream = Stream(auth, self)
        #["GontzalYrth"]
        self.track[0]=self.track[0].decode('utf-8')
        #print self.track[0].decode('utf-8')
        twitterStream.filter(track=self.track,async=self.async)
        return twitterStream

    def stop(self):
        log.msg("Stoping twitter...")
        return False

    def on_data(self,data):
        try:
            parsed=""
            #Convert to JSON the input string
            data_json = json.loads(data)
            log.msg("============================0")
            log.msg("got message from twitter")
            #listener.cosa=pp(data_json)
            try:
              parsed = cyclone.escape.json_decode(data)
            except:
              log.msg("Twitter weird message, ignoring...")
              return True
            tweet_id = parsed['id_str']
            tweet_user = parsed['user']['screen_name']
            tweet_url = "https://twitter.com/"+tweet_user+"/status/"+tweet_id
            htmlcard="<a class=\"embedly-card\" href=\""+tweet_url+"\">Prueba</a>\n"
            nodeId=str(uuid.uuid4())
            htmlcard="<div class=\"node\" id=\"m"+nodeId+"\">"+htmlcard+"</div>"
              #twitter sample url
            htmlcard=htmlcard+html_script
            #htmlcard=TWITTER_SCRIPT_TAG % tweet_url
            node = {
                  "id": nodeId,
                  "html":htmlcard
                  }
            log.msg("Attemptim to update cache twitter")
            #node["html"] = self.render_string("node.html", node=node)
            self.cache.update_cache(node)
            self.cache.send_updates(node)
            log.msg("Cache updated")
            return True
        except ValueError:
            return True
        except:
            log.msg("Error in twitter tracker, still going on...")
            return True

    def on_error(self,status):
        print status
        return False