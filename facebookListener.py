import requests # pip install requests
import json
import urllib
import datetime
import time
import uuid
from twisted.python import log

def pp(o):
   return json.dumps(o, indent=4)
html_script = "<script src=\"//cdn.embedly.com/widgets/platform.js\"></script>"
class facebookListener():

    def __init__(self,track,cache,limit=10):
        self.track = track[:][0]
        self.cache = cache
        self.limit = str(limit)
        self.running = 0
        self.last_id = 0
        self.posts=[]

    def start(self):
        self.running=1
        while self.running==1:
            self.getPosts()
            self.updatePosts(self.posts)

    def stop(self):
        self.running=0
        return False
    def getPosts(self):
        r = requests.get('https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id=512472425538516&client_secret=139fe88d7622e19caf924bd60db160ef')
        access_token = r.text.split('=')[1]
        nextpage = "https://graph.facebook.com/v1.0/search?q="+str(self.track)+"&type=post&limit="+str(self.limit)+"&access_token="+str(access_token)
        f = urllib.urlopen(nextpage)
        s = f.read()
        f.close()
        ss = json.loads(s)
        itera=0
        posts=[]
        for i in ss['data']:
            if self.last_id==i['id']:
                log.msg("We already have that post")
                break
            else:
                if itera==0:
                    self.last_id=i['id']
                    itera=1
                posts.append(i['id'])
        self.posts=posts+self.posts

    def updatePosts(self,posts):
        for ht in posts:
            if self.running==1:
                log.msg("============================>")
                log.msg("got message from facebook")
                nodeId=str(uuid.uuid4())
                htmlcard="<a class=\"embedly-card\" href=\""+"http://www.facebook.com/"+ht+"\">Prueba</a>\n"+html_script
                htmlcard="<div class=\"node\" id=\"m"+nodeId+"\">"+htmlcard+"</div>"
                #twitter sample url
                htmlcard=htmlcard+html_script
                node = {
                    "id": nodeId,
                    "html":htmlcard
                    }
                log.msg("Attemptim to update cache facebook")
                self.cache.update_cache(node)
                self.cache.send_updates(node)
                time.sleep(15)

